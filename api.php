<?php

// PHP Data Objects(PDO) Code:
try {
    $db = new PDO("sqlsrv:server = tcp:gkdbc01.database.windows.net,1433; Database = skulpturapp;ConnectionPooling=0", "gkadmin", "WhiteSwan2017");
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    print("Error connecting to SQL Server.");
    die(print_r($e));
}

// Select everything from the table containing the landmarks information
$sql = "SELECT * FROM landmarks";
$sth = $db->prepare($sql);
$dbresult = $sth->execute();

while ($row = $sth->fetch(PDO::FETCH_ASSOC)) {
  
  $landmarks[] = array(
  "id" => $row['id'],
  "title" => $row['title'],
  "description" => $row['description'],
  "artist" => $row['artist'],
  "theme" => $row['theme'],
  "year" => $row['year'],
  "links" => $row['links'],
  "url" => $row['url'],
  "lat" => $row['lat'],
  "lng" => $row['lng']
  );
  
}

//If the query was executed successfully, create a JSON string containing the marker information
if ($dbresult) {
  $result = '{"success":true, "landmarks":' . json_encode($landmarks) . '}';
} else {
  $result = '{"success":false}';
}

//Set these headers to avoid any issues with cross origin resource sharing issues
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
header('Access-Control-Allow-Headers: Content-Type,x-prototype-version,x-requested-with');

//Output the result to the browser so that our Ionic application can see the data
echo $result;

?>